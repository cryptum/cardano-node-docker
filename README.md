# cardano-node-docker

```bash
# Run Cardano dev
docker-compose -f docker-compose.testnet.yml up -d

# Run Cardano prod
docker-compose -f docker-compose.mainnet.yml up -d
```
